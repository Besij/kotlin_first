package homework4

fun main() {
    val lion = Lion("Simba", 3, 80)
    val tiger = Tiger("shram", 2, 70)
    val hippopotamus = Hippopotamus("motomoto", 3, 700)
    val wolf = Wolf("auf", 2, 50)
    val giraffe = Giraffe("melman", 7, 300)
    val elephant = Elephant("dambo", 5, 1000)
    val chimpanzee = Chimpanzee("rafiki", 1, 30)
    val gorilla = Gorilla("king kong", 1, 30, 0, arrayOf("mango", "blueberry"))

    lion.eat("pepsi")
    println("Лев ${lion.name}, высотой равной ${lion.height}, весом равным ${lion.weight}, съел 'pepsi' и его сытость стала равна ${lion.satiety}")
    lion.eat("meat")
    println("Лев ${lion.name}, высотой равной ${lion.height}, весом равным ${lion.weight}, съел 'meat' и его сытость стала равна ${lion.satiety}")

    gorilla.eat("macbook")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, съел 'macbook' и его сытость стала равна ${gorilla.satiety}")
    gorilla.eat("mango")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, съел 'mango' и его сытость стала равна ${gorilla.satiety}")
    gorilla.eat("mango")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, еще раз съел 'mango' и его сытость теперь стала равна ${gorilla.satiety}")
}

class Lion(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("meat", "bones")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Tiger(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("meat", "bones")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Hippopotamus(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("watermelon", "melon")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Wolf(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("meat", "bones")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Giraffe(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("leafs", "flowers")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Elephant(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("banana", "pineapple")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Chimpanzee(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("banana", "strawberry")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}

class Gorilla(val name: String, var height: Int, var weight: Int) {
    var diet = arrayOf("banana", "mango")
    var satiety: Int = 0

    constructor(name: String, height: Int, weight: Int, food: Array<String>) : this(name, height, weight) {
        this.diet = food
    }

    constructor(name: String, height: Int, weight: Int, satiety: Int, food: Array<String>) : this(
        name,
        height,
        weight
    ) {
        this.satiety = satiety
        this.diet = food
    }

    fun eat(food: String) {
        if (diet.contains(food)) {
            this.satiety += 1
        }
    }
}