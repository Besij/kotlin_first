package homework6

fun main() {
    val userValidLoginValidPassword = authenticationAndCreate("aaaaaaaaaaaaaaaaaaaa", "bbbbbbbbbb", "bbbbbbbbbb")
//    val userValidLoginInvalidPassword = authenticationAndCreate("aaaaaaaaaaaaaaaaaaaa", "bbbbbb", "bbbbbb")
//    val userInvalidLogInvalidPassword = authenticationAndCreate("aaaaaaaaaaaaaaaaaaaaa", "bbbbbbbbbb", "bbbbbbbbbb")
//    val userInvalidLoginInvalidPassword = authenticationAndCreate("aaaaaaaaaaaaaaaaaaaaa", "bbbbbb", "bbbbbb")
//    val userNotSamePasswords = authenticationAndCreate("aaaaaaaaaaaaaaaaaaaa", "bbbbbbbbbb", "cccccccccc")
    println("пользователь с login: ${userValidLoginValidPassword.login} password: ${userValidLoginValidPassword.password} успешно создан")
//    println("пользователь с login: ${userValidLoginInvalidPassword.login} password: ${userValidLoginInvalidPassword.password} успешно создан")
//    println("пользователь с login: ${userInvalidLogInvalidPassword.login} password: ${userInvalidLogInvalidPassword.password} успешно создан")
//    println("пользователь с login: ${userInvalidLoginInvalidPassword.login} password: ${userInvalidLoginInvalidPassword.password} успешно создан")
//    println("пользователь с login: ${userNotSamePasswords.login} password: ${userNotSamePasswords.password} успешно создан")
}

fun authenticationAndCreate(login: String, password1: String, password2: String): User {
    if (login.length > 20)
        throw WrongLoginException("В логине не должно быть более 20 символов")
    if (password1.length < 10)
        throw WrongPasswordException("В пароле не должно быть менее 10 символов")
    if (password1 != password2)
        throw WrongPasswordException("Пароли не совпадают")
    return User(login, password1)
}

class User(val login: String, val password: String) {
}

class WrongLoginException(message: String) : Exception(message)
class WrongPasswordException(message: String) : Exception(message)
