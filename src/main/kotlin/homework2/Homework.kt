package homework2

fun main() {
    val lemonade: Int = 18500
    val pinaColada: Short = 200
    val whiskey: Byte = 50
    val drops: Long = 3_000_000_000
    val cola: Float = 0.5f
    val ale: Double = 0.666666667
    val signature: String = "Что-то авторское!"

    println("Заказ - '$lemonade мл лимонада' готов!")
    println("Заказ - '$pinaColada мл пина колады' готов!")
    println("Заказ - '$whiskey мл виски' готов!")
    println("Заказ - '$drops капель фреша' готов!")
    println("Заказ - '$cola л колы' готов!")
    println("Заказ - '$ale л эля' готов!")
    println("Заказ - '$signature' готов!")
}
