package homework3

fun main() {
    val treasureChest = arrayOf(2, 3, 4, 4, 8, 6, 7)
    var joCounter = 0
    var teamCounter = 0
    for (element in treasureChest) {
        if (element % 2 == 0) {
            joCounter++
        } else {
            teamCounter++
        }
    }
    println("Джо получит - $joCounter монет\nКоманда получит - $teamCounter монет")
}