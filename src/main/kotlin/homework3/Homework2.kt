package homework3

fun main() {
    val journal = intArrayOf(2, 3, 2, 4, 5, 5, 2, 3, 3, 4, 5, 5, 5, 5, 5, 4)
    var aStudentsCount = 0 // отличники
    var bStudentsCount = 0 // хорошисты
    var cStudentsCount = 0 // троечники
    var dStudentsCount = 0 // двоечники
    for (i in journal) when (i) {
        5 -> aStudentsCount++
        4 -> bStudentsCount++
        3 -> cStudentsCount++
        2 -> dStudentsCount++
    }
    println("Отличников - ${(aStudentsCount.toFloat() / journal.size * 100)} %")
    println("Хорошистов - ${(bStudentsCount.toFloat() / journal.size * 100)} %")
    println("Троечников - ${(cStudentsCount.toFloat() / journal.size * 100)} %")
    println("Двоечников - ${(dStudentsCount.toFloat() / journal.size * 100)} %")
}
