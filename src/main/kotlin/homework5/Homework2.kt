package homework5

fun main() {
    val arrayAnimal = arrayOf(
        Lion("Simba", 3, 80),
        Tiger("shram", 2, 70),
        Hippopotamus("motomoto", 3, 700),
        Wolf("auf", 2, 50),
        Giraffe("melman", 7, 300),
        Elephant("dambo", 5, 1000),
        Chimpanzee("rafiki", 1, 30),
        Gorilla("king kong", 1, 30)
    )
    val arrayFood = arrayOf(
        "pepsi", "leaf", "bones", "meat", "banana", "mango", "strawberry", "mango"
    )
    feedAnimals(arrayAnimal, arrayFood)
}

fun feedAnimals(animals: Array<Animal>, foods: Array<String>) {
    for (animal in animals) {
        println("\nЖивотное ${animal.name} начинает кушать:")
        for (food in foods) {
            animal.eat(food)
        }
    }
}


abstract class Animal(
    open val name: String,
    open val height: Int,
    open val weight: Int,
    val diet: Array<String>
) {

    var satiety = 0

    fun eat(food: String) {
        if (diet.contains(food)) {
            satiety++
            println("${name} любит '$food'. Сытость: $satiety")
        } else {
            println("${name} не любит '$food'. Сытость: $satiety")
        }
    }
}



class Lion(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("meat", "bones"))

class Tiger(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("meat", "bones"))

class Hippopotamus(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("watermelon", "melon"))

class Wolf(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("meat", "bones"))

class Giraffe(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("leafs", "flowers"))

class Elephant(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("banana", "pineapple"))

class Chimpanzee(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("banana", "strawberry"))

class Gorilla(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight, arrayOf("banana", "mango"))