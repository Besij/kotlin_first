package homework5

fun main() {
    val lion = Lion("Simba", 3, 80)
    val tiger = Tiger("shram", 2, 70)
    val hippopotamus = Hippopotamus("motomoto", 3, 700)
    val wolf = Wolf("auf", 2, 50)
    val giraffe = Giraffe("melman", 7, 300)
    val elephant = Elephant("dambo", 5, 1000)
    val chimpanzee = Chimpanzee("rafiki", 1, 30)
    val gorilla = Gorilla("king kong", 1, 30)

    lion.eat("pepsi")
    println("Лев ${lion.name}, высотой равной ${lion.height}, весом равным ${lion.weight}, съел 'pepsi' и его сытость стала равна ${lion.satiety}")
    lion.eat("meat")
    println("Лев ${lion.name}, высотой равной ${lion.height}, весом равным ${lion.weight}, съел 'meat' и его сытость стала равна ${lion.satiety}")

    gorilla.eat("macbook")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, съел 'macbook' и его сытость стала равна ${gorilla.satiety}")
    gorilla.eat("mango")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, съел 'mango' и его сытость стала равна ${gorilla.satiety}")
    gorilla.eat("mango")
    println("Горилла ${gorilla.name}, высотой равной ${gorilla.height}, весом равным ${gorilla.weight}, еще раз съел 'mango' и его сытость теперь стала равна ${gorilla.satiety}")
}

abstract class Animal(val name: String, var height: Int, var weight: Int) {
    abstract val diet: Array<String>
    var satiety: Int = 0

    fun eat(food: String) {
        if (diet.contains(food)) {
            satiety++
            println("$name ест '$food'")
        } else {
            println("$name не ест '$food'")
        }
    }
}


class Lion(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("meat", "bones")}

class Tiger(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("meat", "bones")}

class Hippopotamus(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("watermelon", "melon")}

class Wolf(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("meat", "bones")}

class Giraffe(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("leafs", "flowers")}

class Elephant(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("banana", "pineapple")}

class Chimpanzee(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("banana", "strawberry")}

class Gorilla(name: String, height: Int, weight: Int): Animal(name, height, weight) {
    override val diet: Array<String> = arrayOf("banana", "mango")}