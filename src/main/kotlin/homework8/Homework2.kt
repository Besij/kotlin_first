package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей в корзине бабушки: ${sumVegetablesInCart(userCart)}")
    println("Сумма товаров корзины бабушки: ${calculateAmount(userCart)}")
}

fun sumVegetablesInCart(cart: MutableMap<String, Int>): Int {
    var veges = 0
    vegetableSet.forEach {
        if (cart.containsKey(it)) {
            veges += cart.getValue(it)
        }
    }
    return veges
}

fun calculateAmount(basket: MutableMap<String, Int>): Double {
    var amount = 0.0
    val newDiscount = 1 - discountValue
    for ((key, value) in basket) {
        if ((prices.containsKey(key)) && (discountSet.contains(key))) {
            amount += (value * prices[key]!!) * newDiscount
            continue
        }
        if (prices.containsKey(key)) {
            amount += value * prices[key]!!
        }
    }
    return amount
}
